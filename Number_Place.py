import tkinter as tk
import tkinter.ttk as ttk
import Cell
import Cell_Solver

def Initial_SetUP():
    root = tk.Tk()
    field = [ttk.Combobox(root,values=(1,2,3,4,5,6,7,8,9),state='readonly',width=2,height=9) for i in range(81)]
    counter = 0
    for field_index in field:
        field_index.grid(row=int(counter/9),column=int(counter%9))
        counter += 1
        
    button = tk.Button(text="完了",command=lambda:root.quit())
    button.grid(row=10,column=5)
    root.mainloop()
    return field

def Cell_SetUP(field):
    Cells = [Cell.NumPle_Cell() for i in range(81)]
    for cell_index in range(len(Cells)):
        if field[cell_index].get() == "":
            continue
        decide_number = int(field[cell_index].get())
        Cells[cell_index].Decide_put_number(decide_number)
    return Cells

def print_result(field):
    for field_index in field:
        if field_index.get() == "":
            continue
        print(field_index.get())

if __name__ == "__main__":
    field = Initial_SetUP()
    Cells = Cell_SetUP(field)
    solver = Cell_Solver.Solver(Cells)
    solver.God_Scan()