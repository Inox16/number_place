import Cell
import Cell_Solver

class Test:
    def __init__(self):
        #initialize
        self.Cells = [Cell.NumPle_Cell() for i in range(81)]
        self.Cells[0].Decide_put_number(2)
        self.Cells[80].Decide_put_number(4)
        self.solver = Cell_Solver.Solver()
    
    def Vertical_Checker_Test(self):
        self.solver.Vertical_Checker(self.Cells)
        for cell in range(len(self.Cells)):
            if cell%9 == 0:
                print(self.Cells[cell].will_put)

    def Horizontal_Checker_Test(self):
        self.solver.Horizontal_Checker(self.Cells)
        for cell in range(len(self.Cells)):
            if int(cell/9) == 0:
                print(self.Cells[cell].will_put)
    
    def Square_Checker_Test(self):
        self.solver.Square_Checker(self.Cells)
        check_cell = [0,1,2,9,10,11,18,19,20]
        for cell in range(len(self.Cells)):
            if cell in check_cell:
                print(self.Cells[cell].will_put)

if __name__ == "__main__":
    test = Test()
    print("=====================")
    test.Vertical_Checker_Test()
    print("=====================")
    test.Horizontal_Checker_Test()
    print("=====================")
    test.Square_Checker_Test()
    print("=====================")
    array = [1]
    print(len(array))
    print(array[0])