import Cell

class Solver:
    def __init__(self,Cells):
        self.Cells = Cells
        return
    
    def Vertical_Checker(self):
        for v_index in range(9):
            for check_cell in range(9):
                put_number = self.Cells[v_index + check_cell *9].Get_put_number()
                if(put_number == None):
                    continue
                for sweep in range(9):
                    self.Cells[v_index + sweep *9].Delete_will_put(put_number)

    def Horizontal_Checker(self):
        for h_index in range(9):
            for check_cell in range(9):
                put_number = self.Cells[check_cell + h_index *9].Get_put_number()
                if(put_number == None):
                    continue
                for sweep in range(9):
                    self.Cells[sweep + h_index *9].Delete_will_put(put_number)

    def Square_Checker(self):
        for sv_index in range(3):
            for sh_index in range(3):
                base_index = sv_index*27 + sh_index*3
                for v_index in range(3):
                    for h_index in range(3):
                        put_number = self.Cells[base_index + v_index*9 + h_index].Get_put_number()
                        if(put_number == None):
                            continue
                        for v in range(3):
                            for h in range(3):
                               self.Cells[base_index + v*9 + h].Delete_will_put(put_number)
    
    def Decide_Number(self):
        fixed_count = 0
        some_changes = 0
        for cell in range(len(self.Cells)):
            check_code = self.Cells[cell].Check_will_put() 
            #決定済みセルの場合
            if check_code == 0:
                fixed_count += 1
            elif self.Cells[cell].some_changed == True:
                some_changes += 1
                self.Cells[cell].Reset_some_changed()
        return fixed_count,some_changes
    
    def print_Cells(self):
        counter = 0
        for cell in range(len(self.Cells)):
            if counter%9 == 0:
                print()
            print_number = self.Cells[cell].Get_put_number() 
            print(print_number,end="\t")
            counter += 1

    def God_Scan(self):
        fixed_count = 0
        some_changes = 0
        while(True):
            self.Vertical_Checker()             
            self.Horizontal_Checker()
            self.Square_Checker()
            fixed_count,some_changes = self.Decide_Number()
            if fixed_count == 81:
                print(fixed_count)
                break
            elif some_changes == 0:
                print("error")
                break
        self.print_Cells()