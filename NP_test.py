import unittest
import Cell
import Cell_Solver

class Num_Pla_Test(unittest.TestCase):
    def setUp(self):
        #initialize
        self.Cells = [Cell.NumPle_Cell() for i in range(81)]
        self.Cells[0].Decide_put_number(2)
        self.solver = Cell_Solver.Solver()
        pass

    def terDown(self):
        #end
        pass
    #some tests
    def Vertical_Checker_Test(self):
        self.solver.Vertical_Checker(self.Cells)
        for cell in range(len(self.Cells)):
            if cell%9 == 0:
                self.assertEqual(2,self.Cells[cell].Get_put_number())
    
    def Cells_Printer(self):
        counter = 0
        for cell in self.Cells:
            if counter%9 == 0:
                print()
            print_number = cell.Get_put_number()
            print(print_number,end="")

if __name__ == "__main__":
    unittest.main()