class NumPle_Cell:
    def __init__(self):
        self.fixed = False
        self.will_put = [1,2,3,4,5,6,7,8,9]
        self.put_number = None
        self.some_changed = False
    def Decide_put_number(self,put_number):
        self.put_number = put_number
        self.will_put.clear()
        self.fixed = True
    def Delete_will_put(self,number):
        if number in self.will_put:
            self.will_put.remove(number)
            self.some_changed = True
        return
    def Check_will_put(self):
        if self.fixed == True:
            return 0
        if len(self.will_put) == 1:
            self.Decide_put_number(self.will_put[0])
            return 1
        return -1
    def Get_put_number(self):
        return self.put_number
    def Reset_some_changed(self):
        self.some_changed = False